%define debug_package %{nil}

Name:           terramate
Version:        0.11.6
Release:        1%{?dist}
Summary:        Terramate is a tool for managing multiple Terraform stacks that comes with support for change detection and code generation.

License:        ASL 2.0
URL:            https://github.com/terramate-io/terramate
Source0:        https://github.com/terramate-io/%{name}/releases/download/v%{version}/%{name}_%{version}_linux_x86_64.tar.gz

%description
Terramate is a tool for managing multiple Terraform stacks 
that comes with support for change detection and code generation.

%prep
%autosetup -n %{name} -c

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/usr/bin
install -p -m 755 %{name} %{buildroot}/usr/bin

%files
/usr/bin/terramate

%changelog
* Mon Jan 06 2025 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.11.6

* Sun Mar 03 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.4.6

* Thu Dec 28 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.4.3

* Wed Nov 15 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.4.2

* Wed Jul 26 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.4.0

* Mon Jun 19 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.3.0

* Mon May 08 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.2.18

* Tue Apr 25 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.2.17

* Mon Feb 27 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.2.13

* Sat Feb 25 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.2.12

* Fri Feb 10 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.2.11

* Wed Dec 14 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.2.4

* Fri Nov 25 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.2.1

* Tue Oct 15 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.2.0

* Tue Oct 01 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.1.40

* Wed Sep 28 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.1.34

* Mon Sep 26 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.1.33

* Sat Sep 24 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 0.1.32

* Sun Sep 11 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Inital RPM
